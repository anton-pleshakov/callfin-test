<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="csrf-token" content="{{ csrf_token() }}" />
  <title>Book Catalog</title>
  <script src="{{asset('/js/jquery-2.2.4.min.js')}}"></script>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css">
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
  <link rel="stylesheet" href="{{asset('/css/css/style.css')}}">
  <script src="{{asset('/js/ajax.js')}}"></script>

</head>
<body>
{{------Modal Window Begin------}}
<div id="myModal" class="modal fade">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <!-- <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button> -->

        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h4 class="modal-title" id="select-statement-title">Добавление новой книги</h4>

      </div>
      <div class="modal-body">
        <p>* Заполните обязательные поля</p>
        <form id="add-book-form" method="POST" class="validateform">
          {{ csrf_field() }}

          <div class="row">
            <div class="col-lg-12">
              <input type="text" class="form-control" name="title" placeholder="Название книги*" required />
            </div>
            <div class="col-lg-12">
              <input type="text" class="form-control" name="author" placeholder="Имя автора*" required />
            </div>
            <div class="col-lg-12">
              <input type="text" class="form-control" name="publish" placeholder="Издательство"  />
            </div>
            <div class="col-lg-12">
              <input placeholder="Дата выхода" type="text" class="add-book-date form-control" name="date" onfocus="(this.type='date')">
            </div>
            <p class="add-bool-alert">Книга успешно добавлена!</p>

          </div>
        </form>

      </div>
      <div class="modal-footer">
        <button class="btn btn-primary margintop10 pull-left" type="submit" form="add-book-form">Добавить</button>
        {{--<button type="button" class="btn btn-primary" data-dismiss="modal">Continue</button>--}}
      </div>
    </div>
  </div>
</div>
{{------Modal Window End------}}

<div class="jumbotron">
  <div class="container">
    <h1>Книжный каталог</h1>
    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">Добавить книгу</button>
  </div>
</div>

  @yield('content')

<footer>
  <p>&copy; 2016 Company, Inc.</p>
</footer>

</body>
</html>