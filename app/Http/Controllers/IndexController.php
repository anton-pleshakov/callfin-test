<?php

namespace App\Http\Controllers;

use App\Book;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image as ImageInt;
use App\Http\Controllers\DB;

class IndexController extends Controller
{

    public function index()
    {
        $books = Book::select('id','title', 'author', 'publish', 'date','img')->get();
        return view('index')->with(['books' => $books]);
    }

    public function update(Request $request, $id)
    {
        $path = public_path().'\upload\\';
        $file = $request->file('file');
        if($file !=null){

            $filename = str_random(20) .'.' . $file->getClientOriginalExtension() ?: 'png';
            $img = ImageInt::make($file);
            $img->resize(144,206)->save($path . $filename);

            Book::where('id', $id)->first()->update(['img' => $filename]);

            return redirect('/');
        }
        else{
            return redirect('/');
        }

    }




}
