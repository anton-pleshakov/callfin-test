<?php

namespace App\Http\Controllers;

use App\Book;
use Illuminate\Http\Request;

class IndexController extends Controller
{
    //

    public function index(){

        $books = Book::all();
        dump($books);
        return view('index')->with(['books' => $books]);

    }

}
