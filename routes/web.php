<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'IndexController@index')->name('indexRoute');

Route::post('/add', 'AjaxController@addBook')->name('addBook');

Route::any('/images/{book}', 'IndexController@update')->name('addImage');

Route::delete('/book/delete/{id}', 'AjaxController@deleteBook');

